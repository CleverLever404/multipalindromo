# Multipalindromo

Multipalindrome generator (spanish)

Un palíndromo es una frase o palabra en la que puede leerse lo mismo en un sentido u otro, como en la palabra "Ananá".
El concepto de multipalindromo es muy interesante. Es ejemplo mas conocido es [el cuadrado Sator](https://en.wikipedia.org/wiki/Sator_Square), que está en latin.


![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Sator_Square_at_Opp%C3%A8de.jpg/220px-Sator_Square_at_Opp%C3%A8de.jpg)

A diferencia de un palíndromo, en un multipalíndromo la frase no sólo puede leerse de izquierda a derecha y vice versa, sino también de arriba hacia abajo y vice versa. Por eso también las palabras se disponen en una grilla y no sobre un único renglón.

Existen distintos ejemplos de esto en este [link](https://en.wikipedia.org/wiki/Word_square).

## Objetivo

A partir de la información anterior, se pensó en armar un generador de multipalíndromos en español a partir de una librería de palabras que existen en español.
Ya teniendo la lista de palabras, para armar la grilla final es importante tener en cuenta que todas las palabras deben tener la misma cantidad de letras. Y ésto también determina de cuántas palabras se compondrá el cuadrado.
Si la cantidad de palabras de la frase es impar, la palabra central deberá ser un palíndromo en sí misma. Así como lo es TENET en el Cuadrado Sator.
Además, las demás palabras deberán ser bifrontes entre sí, generando pares bifrontes con la palabra central como eje de simetría. Entonces en el Cuadrado Sator, estas palabras serían SATOR-ROTAS y AREPO-OPERA.


## Uso
Al ejecutar el script de python se deberia ver impreso en pantalla los multipalindormos encontrados por fuerza bruta.


