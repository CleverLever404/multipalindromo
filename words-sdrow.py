#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan  9 16:45:49 2022

@author: chimaka
"""
#%%
import numpy as np

# load data and sort

Words = np.loadtxt("spawords.txt", dtype='str')
Words = np.unique(Words)
nW = len(Words)

print("son ", nW, "palabras")

# generate list of inverted words
Sdrow = np.unique([W[::-1] for W in Words])



#%%

bifrontas = []

for W in Words:
    indice = np.searchsorted(Sdrow, W)

    if nW <= indice:
        continue

    M = Sdrow[indice]

    if W == M:
        par = np.sort([W, M[::-1]])
        bifrontas.append([len(W), *par])
        print(bifrontas[-1])

bifrontas = np.unique(bifrontas, axis=0)

# %%

nLetras = 5

indices = bifrontas[:,0] == str(nLetras)

for bif in bifrontas[indices]:
    if bif[1] == bif[2]:
        print(bif[1], bif[2], "palindromo")
    else:
        print(bif[1], bif[2])
